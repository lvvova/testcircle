using System;
using System.Collections;
using UnityEngine;
using DG.Tweening;
using strange.extensions.mediation.impl;

namespace CircleIoC
{
    class PieceHolderView : CircleView
    {
        [SerializeField] ParticleSystem reactEffect;
        internal const string CLICK_ON_CIRCLE_EVENT = "CLICK_ON_CIRCLE_EVENT";

        public void ReactOnAction()
        {
            reactEffect.Play();

            Sequence mySeq = DOTween.Sequence();
            mySeq.Append(transform.DOScale(0.85f, 0.1f));
            mySeq.Append(transform.DOScale(1f, 0.5f));
        }

        internal void init()
        {
            gameObject.AddComponent<ClickDetector>();
            ClickDetector clicker = gameObject.GetComponent<ClickDetector>();
            clicker.dispatcher.AddListener(ClickDetector.CLICK, onClick);
        }

        void onClick()
        {
            dispatcher.Dispatch(CLICK_ON_CIRCLE_EVENT);
        }

        internal void circleClicked()
        {
            ReactOnAction();
        }
    }
}