﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
namespace Game
{
    public class GameManager : MonoBehaviour
    {
        //Список всех кусков
        [SerializeField] List<PieceHolder> pieceHolders;
        [SerializeField] Circle baseHolder;
        //Лист с шансом дропа определённого колва кругов
        [SerializeField] List<float> pieceDropCountChances;
        //Здесь находится префаб куска
        [SerializeField] GameObject piece;

        void OnEnable()
        {
            PieceHolder.pieceProcessedSuccessful += StartNewGameCycle;
        }

        void OnDisable()
        {
            PieceHolder.pieceProcessedSuccessful -= StartNewGameCycle;
        }

        void Start()
        {
            StartNewGameCycle();
        }

        private void StartNewGameCycle()
        {
            baseHolder.pieceData = generatePieceData();
            baseHolder.pieceData.visualPices = generatePieceView(baseHolder.pieceData);
            baseHolder.IncertPieces(baseHolder.pieceData);
            CheckGameOver();

        }

        /*
        Метод генерирует данные для набора сегментов
        Предусмотренна возможность генерации сразу несоколько сегментов
        Для этого я создал лист в котором по порядку описаны шансы создания нескольких элементов 
        */
        private PieceData generatePieceData()
        {
            PieceData pieceData = new PieceData();
            //Через цикл нахожу количество объектов которые нужно заспавнить
            for (int i = 0; i < pieceDropCountChances.Count; i++)
            {
                //Если шанс дропа выпадает нужный тогда цикл начинается иначе повторяем до упора
                //Шанс 1: 100% 2: 20% 3: 5%  в массиве выставленны в обратном порядке
                if (Random.Range(0, 1f) < pieceDropCountChances[i])
                {
                    //Создаю 1 кусок
                    var randomPoint = Random.Range(0, pieceData.pieces.Length);
                    pieceData.pieces[randomPoint] = 1;
                    //Если есть возможность добавляю к нему дополнительные куски;
                    for (int j = 0; j < Mathf.Abs(i - pieceDropCountChances.Count); j++)
                    {
                        pieceData.pieces[returnMoreOrLessSix(randomPoint + j)] = 1;
                    }
                    break;
                }
            }
            return pieceData;
        }

        //Генерирую внешний вид для кусков
        private List<GameObject> generatePieceView(PieceData pieceData)
        {
            List<GameObject> pieceList = new List<GameObject>();
            //Проверяю каждую ячейку на наличие 1
            for (int i = 0; i < pieceData.pieces.Length; i++)
            {
                //Если в дате кусок есть (позначен как 1) тогда создаю кусок 
                if (pieceData.pieces[i] == 1)
                {
                    //Кусок будет повёрнут на определёный градус, это завист от позиции в масивн
                    // Т.е если кусок в 3 ячейке тогда его угол будет 3 * 60 = 180
                    var spawnedPiece = Instantiate(piece, transform.position, Quaternion.identity, null) as GameObject;
                    spawnedPiece.transform.eulerAngles = new Vector3(0, 0, i * 60);
                    pieceList.Add(spawnedPiece);
                }
            }
            return pieceList;
        }
        void CheckGameOver()
        {
            //Беру данные о новом куске чтобы проверить есть ли возможность вставить его куда либо 
            var currentPiece = baseHolder.pieceData;
            int cantSendCounter = 0;
            //Массив всех кругов;
            foreach (var item in pieceHolders)
            {
                //Проверяю есть ли возможность вставить в конкретный круг новый кусок
                if (checkSame(item.pieceData, currentPiece))
                {
                    //Если нет тогда прибавляю;
                    cantSendCounter++;
                }
            }
            //Если новый кусок не подходит в все 6 кругов тогда игра закончена
            if (cantSendCounter == 6)
            {
                GameOver();
            }
        }

        private void GameOver()
        {
            Debug.LogWarning("NotImplementedException");
            //Тупо начинаю сцену с 0;
            Debug.Log("Game over, Restarting");
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        //Проверяет 2 даты на возможность вставки
        bool checkSame(PieceData firstPieces, PieceData secondPieces)
        {
            bool haveSameSegment = false;
            for (int i = 0; i < firstPieces.pieces.Length; i++)
            {
                //Если 1 дата имеет в текущей позиции 1 тогда проверяю на разность
                if (firstPieces.pieces[i] == 1)
                {
                    //Если в текущей позиции обе даты имеют 1 тогда делается вывод о том что даты "не совместимы"
                    if (firstPieces.pieces[i] == secondPieces.pieces[i])
                    {
                        haveSameSegment = true;
                        break;
                    }
                }
            }
            return haveSameSegment;
        }

        public List<PieceHolder> ReturnPiceceHolders() => pieceHolders;

        int returnMoreOrLessSix(int val)
        {
            if (val < 0)
                return val + 6;
            else if (val > 5)
                return val - 6;
            else
                return val;
        }
    }

}
