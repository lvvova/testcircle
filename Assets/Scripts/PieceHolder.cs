﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
namespace Game
{
    public class PieceHolder : Circle
    {
        //Экшн сообщает что данные удачно сохранены, отправялет трансформ котнейнера этого круга в центральный круг
        public static System.Action pieceProcessedSuccessful;
        //Экшн сообщает что отправка обработана успешно, отправляю игроку евент чтобы он мог продолжить отправлять сегменты
        public static System.Action attemptToSendCompleted;
        public static System.Action<PieceHolder> circleIsFull;
        //партикл
        [SerializeField] ParticleSystem reactEffect;

        //Проверяю на возможность вставки кусков  
        public void CheckForInsertion(PieceData takenPiece)
        {
            //Реакция на то что объект был задействован
            ReactOnAction();
            //Проверка на возможность вставки кусков
            if (checkSame(takenPiece, pieceData))
            {
                //Если не возможно тогда отправляю инфу о не удачной попытке вставки
                ReturnBack(takenPiece);
            }
            else
            {
                //Если возможно вставляю
                IncertPieces(takenPiece);
                pieceProcessedSuccessful?.Invoke();
            }
            attemptToSendCompleted?.Invoke();
            checkFullCircle();
        }

        private void checkFullCircle()
        {
            if (System.Array.TrueForAll(pieceData.pieces, x => x == 1))
            {
                //Если все элементы равны 1 тогда уничтожаю
                circleIsFull?.Invoke(this);
            }
        }

        private void ReturnBack(PieceData takenPiece)
        {
            Debug.LogWarning("NotImplementedException");
        }

        public void ReactOnAction()
        {
            //Запускаю партикл
            reactEffect.Play();
            //Анимация через дотвин
            Sequence mySeq = DOTween.Sequence();
            //Сначала объект уменьшаю
            mySeq.Append(transform.DOScale(0.85f, 0.1f));
            //Потом увеличиваю обратно
            mySeq.Append(transform.DOScale(1f, 0.5f));
        }

        //Проверяет 2 даты на возможность вставки
        bool checkSame(PieceData firstPieces, PieceData secondPieces)
        {
            bool haveSameSegment = false;
            for (int i = 0; i < firstPieces.pieces.Length; i++)
            {
                //Если 1 дата имеет в текущей позиции 1 тогда проверяю на разность
                if (firstPieces.pieces[i] == 1)
                {
                    //Если в текущей позиции обе даты имеют 1 тогда делается вывод о том что даты "не совместимы"
                    if (firstPieces.pieces[i] == secondPieces.pieces[i])
                    {
                        haveSameSegment = true;
                        break;
                    }
                }
            }
            return haveSameSegment;
        }
        public Transform GetPieceContainer() => pieceContainer;
    }

}
