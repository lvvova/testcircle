﻿using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.context.impl;

namespace CircleIoC
{
    public class circleRoot : ContextView
    {
        void Awake()
        {
            //Instantiate the context, passing it this instance.
            context = new circleContext(this);
        }
    }
}
