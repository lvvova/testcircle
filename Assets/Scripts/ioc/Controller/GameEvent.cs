namespace CircleIoC
{
    public enum GameEvent
    {
        GAME_OVER,
        GAME_UPDATE,
        RESTART_GAME,
        CIRCLE_FULL
    }
}