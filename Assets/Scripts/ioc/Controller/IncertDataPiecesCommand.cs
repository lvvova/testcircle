using System;
using UnityEngine;
using strange.extensions.command.impl;

namespace CircleIoC
{
    public class IncertDataPiecesCommand : EventCommand
    {
        [Inject(CircleType.Center)]
        IPieceData centerPieceData { get; set; }
        public override void Execute()
        {
            IPieceData pieceData = evt.data as IPieceData;

            for (int i = 0; i < pieceData.pieces.Length; i++)
            {
                if (centerPieceData.pieces[i] == 1)
                {
                    //Если дата имеет 1 вставляю её в текущую дату 
                    pieceData.pieces[i] = centerPieceData.pieces[i];
                }
            }
        }
    }
}