using System;
using UnityEngine;
using strange.extensions.dispatcher.eventdispatcher.impl;
using strange.extensions.mediation.impl;

namespace CircleIoC
{
    public class CircleMediator : EventMediator
    {
        [Inject]
        public CircleView view { get; set; }

        public override void OnRegister()
        {
            view.init();
        }
    }
}
