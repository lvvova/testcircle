﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    //Класс был создан для того чтобы оперировать не просто int[] а конкретно сегментами круга
    [System.Serializable]
    public class PieceData
    {
        //6 сегментов в круге
        public int[] pieces;
        //Решил к писДате добавить визуальные куски чтобы было проще отправлять их
        public List<GameObject> visualPices;

        public PieceData()
        {
            pieces = new int[6];
            visualPices = new List<GameObject>();
        }
    }

}
