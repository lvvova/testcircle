﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;

public class LoadAssets : MonoBehaviour
{

    int version = 0;
    [SerializeField]
    AudioSource audioSource;
    [SerializeField]
    List<SpriteRenderer> sRenderers;

    public void DownloadImage()
    {
        string assetBundleName = "pieceimagecontent";
        //С локали
        var loadedAssetBundle =
            AssetBundle.LoadFromFile(Path.Combine(Application.dataPath, "AssetBundles/content/" + assetBundleName));
        if (loadedAssetBundle == null)
        {
            Debug.LogError("Asset load failed");
            return;
        }
        string picName = "PieceHint_00.png";

        var loadedImg = loadedAssetBundle.LoadAsset<Sprite>(picName);

        foreach (var item in sRenderers)
        {
            item.sprite = loadedImg;
        }
    }
}
