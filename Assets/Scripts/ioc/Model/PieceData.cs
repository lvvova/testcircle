using System.Collections.Generic;
using UnityEngine;

namespace CircleIoC
{
    class PieceData : IPieceData
    {
        public PieceData()
        {
            pieces = new int[6];
            visualPieces = new List<GameObject>();
        }

        public int[] pieces { get; set; }
        public List<GameObject> visualPieces { get; set; }
    }
}