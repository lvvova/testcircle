using UnityEngine;
using System.Collections.Generic;


public interface IPieceData
{
    int[] pieces { get; set; }
    List<GameObject> visualPieces { get; set; }
}