﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class Circle : MonoBehaviour
    {
        public PieceData pieceData;
        [SerializeField] protected Transform pieceContainer;

        void Start()
        {
            pieceData = new PieceData();
        }
        public void IncertPieces(PieceData incertedPieceData)
        {
            //Для каждого куска
            for (int i = 0; i < pieceData.pieces.Length; i++)
            {
                if (incertedPieceData.pieces[i] == 1)
                {
                    //Если дата имеет 1 вставляю её в текущую дату 
                    pieceData.pieces[i] = incertedPieceData.pieces[i];
                }
            }

            foreach (var item in incertedPieceData.visualPices)
            {
                item.transform.parent = pieceContainer;
                item.transform.localPosition = Vector3.zero;
                item.transform.localScale = pieceContainer.localScale;
            }
        }
    }

}
