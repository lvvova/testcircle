﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game
{
    public class DestroyManager : MonoBehaviour
    {

        void OnEnable()
        {
            PieceHolder.circleIsFull += ManageDestroying;
        }
        void OnDisable()
        {
            PieceHolder.circleIsFull -= ManageDestroying;
        }

        //Если круг обраружил что он имеет 6 кусков тогда уничтожаю его и 2 его соседей
        void ManageDestroying(PieceHolder pieceHolder)
        {
            //Из гейм менеджера получаю список колец
            var circleArray = GetComponent<GameManager>().ReturnPiceceHolders();
            //В нём ищу позицию этого элемента
            var listPos = circleArray.IndexOf(pieceHolder);

            List<PieceHolder> DestroyCircleList = new List<PieceHolder>();
            //Создаю список колец которые нужно уничтожить
            for (int i = -1; i < 3 - 1; i++)
            {
                DestroyCircleList.Add(circleArray[returnMoreOrLessSix(listPos + i)]);
            }
            //Уничтожаю
            foreach (var item in DestroyCircleList)
            {
                DestroyPieces(item);
            }
        }

        void DestroyPieces(PieceHolder pieceHolder)
        {
            //Очищаю данные о кусочках, кусков теперь нет
            pieceHolder.pieceData = new PieceData();
            //Через цикл нахожу все куски(визуальные) и унчитожаю их
            for (int i = 0; i < pieceHolder.GetPieceContainer().childCount; i++)
            {
                var obj = pieceHolder.GetPieceContainer().GetChild(i);
                Destroy(obj.gameObject);
            }
            //Реакция на то что объект был задействован
            pieceHolder.ReactOnAction();
        }

        int returnMoreOrLessSix(int val)
        {
            if (val < 0)
                return val + 6;
            else if (val > 5)
                return val - 6;
            else
                return val;
        }
    }
}
