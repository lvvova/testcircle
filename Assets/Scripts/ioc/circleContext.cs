﻿using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.dispatcher.eventdispatcher.impl;

namespace CircleIoC
{
    public class circleContext : MVCSContext
    {
        public circleContext(MonoBehaviour view) : base(view)
        {
        }

        public circleContext(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
        {
        }

        protected override void mapBindings()
        {
            //Центральное кольцо
            injectionBinder.Bind<IPieceData>().To<PieceData>().ToName(CircleType.Center).ToSingleton();


            // injectionBinder.Bind<IExampleService>().To<ExampleService>().ToSingleton();


            // mediationBinder.Bind<ExampleView>().To<ExampleMediator>();
        }
    }
}

