using System;
using System.Collections;
using UnityEngine;
using strange.extensions.mediation.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;

namespace CircleIoC
{
    public class CircleView : EventView
    {
        [SerializeField] protected Transform pieceContainer;

        internal void init()
        {

        }

        public void IncertPieces(IPieceData incertedPieceData)
        {
            foreach (var item in incertedPieceData.visualPieces)
            {
                item.transform.parent = pieceContainer;
                item.transform.localPosition = Vector3.zero;
                item.transform.localScale = pieceContainer.localScale;
            }
        }
    }
}
