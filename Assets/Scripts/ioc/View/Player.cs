using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CircleIoC
{
    public class Player : MonoBehaviour
    {

        Camera cachedCamera;
        int circleLayer;

        bool readyToPushPiece;

        void Awake()
        {
            readyToPushPiece = true;
            cachedCamera = Camera.main;
            circleLayer = 1 << 8; //Clickable layer в нём находятся круги куда нужно отправлять кусочки
        }

        void Update()
        {
            Vector2 mousePosition = cachedCamera.ScreenToWorldPoint(Input.mousePosition);

            if (Input.GetMouseButtonDown(0))
            {
                //Если есть возможность нажать тогда:
                if (readyToPushPiece)
                {
                    RaycastHit2D hit = Physics2D.Raycast(mousePosition, Vector2.zero, Mathf.Infinity, circleLayer);
                    if (hit.transform != null)
                    {
                        //Запрещаю отправку сегментов
                        readyToPushPiece = false;
                        //Отправляю в нажатый круг данные о кусочках в центральном круге 
                        //hit.transform.gameObject.GetComponent<IPieceData>().CheckForInsertion(baseHolder.pieceData);
                    }
                }
            }
        }
    }
}